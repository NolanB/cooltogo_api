# API_cooltogo
Deploying to Heroku


## Usage

Setup the project:

1. `pipenv install -d`
2. `pipenv shell`


Setup the database:

1. `sudo docker-compose up-d`
2. `flask init-db`
3. `flask create-user admin test admin`
4. `flask run`
5. Open [http://localhost:5000](http://localhost:5000)
