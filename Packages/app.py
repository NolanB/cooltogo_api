from flask import Flask, render_template, request, redirect, url_for, make_response,g,abort,session
import os
from .DB import BDfonction as db
import functools, os
import click
from .DB import DBInit ,DBTab
from http import HTTPStatus

secret_key = os.environ.get("SECRET_KEY")

def get_db_conn():

    if "conn" not in g:
        g.conn = db.connection()
    return g.conn


def close_db_conn(e=None):

    conn = g.pop("conn", None)
    if conn:
        db.DeconnexionBD(conn)

def load_logged_in_user():
    user_id = session.get("user.id")

    if user_id is None:
        g.user = None
    else:
        conn = get_db_conn()
        g.user = db.get_utilisateur(conn, user_id) 

def load_layout():
    user_role = session.get("user.role")
    if g.user is not None and user_role == 'admin':
        session.layout = "layout_admin.html"
    

app = Flask(__name__)
app.secret_key = secret_key
app.before_request(load_logged_in_user)
app.before_request(load_layout)

app.teardown_appcontext(close_db_conn)

############################# COMMANDE FLASK ############################################################################
#########################################################################################################################
################################################################################################################


@app.cli.command("init-db")
def init_db_command():
    conn = get_db_conn()
    db.create_db_schema(conn)
    DBInit.InitialisationDB(conn)
    click.echo("Initialized the database.")


@app.cli.command("drop-db")
def drop_db_command():
    conn = get_db_conn()
    db.drop_db_schema(conn)
    DBInit.DropTableDB(conn)
    click.echo("Dropped the database.")


@app.cli.command("create-user")
@click.argument("username")
@click.argument("password")
@click.argument("role")
def create_user_command(username, password, role):
    conn = get_db_conn()
    db.create_user(conn, username, password, role)
    click.echo(f"User {username} created with role {role}")


@app.cli.command("generate-secret-key")
@click.argument("nbytes", default=32)
def generate_secret_command(nbytes):
    import secrets

    new_secret_key = secrets.token_hex(nbytes)
    click.echo(f"New secret key: {new_secret_key}")


@app.cli.command("decode-session-cookie")
@click.argument("session_cookie")
def decode_session_cookie_command(session_cookie):
    from flask.sessions import SecureCookieSessionInterface

    ssci = SecureCookieSessionInterface()
    serializer = ssci.get_signing_serializer(app)
    session_cookie_values = serializer.loads(session_cookie)

    click.echo(f"Session cookie values: {session_cookie_values}")


########################## decorateur ########################################################
################################################################################################################


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(render_template("pages/login.html"))
            #abort(HTTPStatus.UNAUTHORIZED)
        return view(**kwargs)

    return wrapped_view

def role_required(role):
    def role_required_decorator(view):
        @functools.wraps(view)
        def wrapped_view(**kwargs):
            if g.user is not None and g.user["role"] == role:
                return view(**kwargs)
            abort(HTTPStatus.FORBIDDEN)

        return wrapped_view

    return role_required_decorator

    
##############################    APP ROUTE     ######################################

@app.route("/", methods=["GET"])
def get_homepage():
    return render_template("pages/login.html")

@login_required
@role_required("admin")
@app.route("/admin", methods=["GET"])
def get_inscription():

    return render_template("pages/inscription.html")


@login_required
@role_required("admin")
@app.route("/inscription", methods=["POST"])
def post_inscription():
    conn= get_db_conn() 
    pseudo = request.form["uname"]
    password = request.form["psw"]
    password_repeat = request.form.get("psw-repeat")

    if password_repeat == password:
        
        try:
            db.create_user(conn=conn, username=pseudo, password=password, role="user")
        except ValueError:
            abort(HTTPStatus.BAD_REQUEST)

        return redirect(url_for("get_inscription"))
    else:
        return render_template('pages/inscription.html', errorMessage="Vos 2 passwords sont différents !")


@app.route('/home', methods=['GET'])
@login_required
def get_home():
    
    return render_template('pages/recherche_lieux.html')
    #return render_template('pages/homepage.html')


@app.route("/login", methods=["POST"])
def post_login():

    pseudo = request.form.get("uname")
    password = request.form.get("psw")
    conn= get_db_conn()
    user_role= session.get('user.role')
    try:
        
        user = db.authentification_utilisateur(conn=conn, username=pseudo, password=password)
    except ValueError:
        return render_template('pages/login.html', errorMessage="Vos identifiant sont incorect !")
    user_id = user[0]
    user_role = user[3]
    session.clear()
    session['user.pseudo']=user[1]

    response = None
    if user_role == db.UserRoleEnum.user:
        response = redirect(url_for("get_home"))
    elif user_role == db.UserRoleEnum.admin:
        response = redirect(url_for("get_inscription"))
    else:
        abort(HTTPStatus.FORBIDDEN)

    session["user.id"] = user_id
    session["user.role"]=user_role
    return response

@app.route("/modif", methods=["GET"])
@login_required
def set_modif():
    session['lieu']= request.args.get('recherche')
    
    fraicheur = db.liste_fraicheur()
    categorie = db.liste_categorie()
    categorie_user = db.liste_categorie_user()
    return render_template('pages/homepage.html',fraicheur=fraicheur,categorie=categorie,categorie_user=categorie_user)
    
@app.route("/modifs", methods=["POST"])
@login_required
def modif():
    #lieu= request.args.get('recherche')
    fraicheur= request.form.get('fraicheur')
    categorie= request.form.get('categorie')
    categorie_user= request.form.get('categorie_user')
    lieu=list(session.get('lieu'))
    
    if categorie != None and fraicheur !=None:
    #fonction update de la base pour fraicheur ...
        db.update_fraicheur(lieu[0],fraicheur)
        db.inserer_categorie_user(lieu[0],categorie_user)
        db.inserer_categorie(lieu[0],categorie)
        return render_template('pages/recherche_lieux.html',errorMessage=f'Vous avez bien modifié les données de {lieu} ')
    else:
        return render_template('pages/homepage.html',errorMessage='Vous avez oublier de renseiger un champs')

@app.route("/logout", methods=["GET"])
@login_required
def get_logout():
    session.clear()
    return redirect(url_for("get_homepage"))

@login_required
@app.route('/recherche', methods=['GET'])
def recherche_titre():
    recher=request.args.get("recherche")
    lieux = db.recherche(recher)
    return render_template("pages/resultat_lieux.html",lieux=lieux)


@login_required
@role_required("admin")
@app.route('/ajout_fraicheur', methods=['POST'])
def ajout_fraicheur():
    conn=db.connection()
    cur=conn.cursor()
    frais=request.form.get("fraicheur")
    dico= {
        'Type': frais
    }

    sql_user = "select Type from Tab_fraicheur"

    cur.execute(sql_user)
    liste_user=cur.fetchall()
    if (dico['Type'],) not in liste_user:
        db.insert_assoc_table(conn,DBTab.InsTab_Fraicheur,dico)
        return render_template("pages/ajout_fraicheur.html",errorMessage='reussit')
    else:
        return render_template("pages/ajout_fraicheur.html",errorMessage='echec')
@login_required
@role_required("admin")
@app.route('/ajout_categorie', methods=['POST'])
def ajout_categorie():
    conn=db.connection()
    cur=conn.cursor()

    dico= {
        'Nom':request.form.get("categorie"),
        "FKId_Fraicheur":request.form.get("fraicheur")

    }
    sql_user = "select Nom from Tab_categorie_lieux"

    cur.execute(sql_user)
    liste_user=cur.fetchall()
    if (dico['Nom'],) not in liste_user:
        db.insert_assoc_table(conn,DBTab.InsTab_Categorie_Lieux,dico)
        return render_template("pages/ajout_categorie.html",errorMessage="reussit")
    else:
        return render_template("pages/ajout_categorie.html",errorMessage="echec")

@login_required
@role_required("admin")
@app.route('/ajout_categorie_user', methods=['POST'])
def ajout_categorie_user():
    conn=db.connection()
    cur=conn.cursor()

    dico= {
        'Nom':request.form.get("categorie_user")
    }
    sql_user = "select Nom from Tab_categorie_user"

    cur.execute(sql_user)
    liste_user=cur.fetchall()
    if (dico['Nom'],) not in liste_user:
        db.insert_assoc_table(conn,DBTab.InsTab_Categorie_User,dico)
        return render_template("pages/ajout_categorie_user.html",errorMessage="ajout user")
    else:
        return render_template("pages/ajout_categorie_user.html",errorMessage="echec ajout user")
@role_required("admin")
@login_required
@app.route('/categorie', methods=['GET'])
def get_categorie():
    fraicheur=db.liste_fraicheur()
    return render_template('pages/ajout_categorie.html',fraicheur=fraicheur)


@role_required("admin")
@login_required
@app.route('/user', methods=['GET'])
def get_user():
    
    return render_template('pages/ajout_categorie_user.html')

@role_required("admin")
@login_required
@app.route('/fraicheur', methods=['GET'])
def get_fraicheur():
    
    return render_template('pages/ajout_fraicheur.html')