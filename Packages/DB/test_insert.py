import pytest
from Packages.DB.BDfonction import connection, create_tab, insert_table, drop_table, DeconnexionBD
from Packages.DB.DBTab import Tab_Lieux, Tab_Villes, Tab_Fraicheur, Tab_Categorie_Lieux, Tab_Categorie_User, Asso_Lieux_C_User, Asso_C_Lieux_Lieux

@pytest.fixture()
def connexion():
    conn = connection()
    yield conn
    DeconnexionBD()


def test_connexion():
    assert connection()!= None
# def test_create_connection():
#     assert connection()


def test_create_tab(connexion):
    with pytest.raises("La création de table a réussi"):
        create_tab(conn, Tab_Villes)
        create_tab(conn, DBTab.Tab_Fraicheur)
        create_tab(conn, DBTab.Tab_Categorie_Lieux)

    


# def user_factory(conn, username, password, role):
#     from werkzeug.security import generate_password_hash

#     query = """
#         INSERT INTO  (username, password_hash, role)
#         VALUES (:username, :password_hash, :role)
#     """

#     params = {
#         "username": username,
#         "password_hash": generate_password_hash(password),
#         "role": role,
#     }

#     user_id = None
#     with conn:
#         cur = conn.execute(query, params)
#         user_id = cur.lastrowid

#     return user_id