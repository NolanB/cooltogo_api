################ Tables étape 1 ################

Tab_Lieux = """
  CREATE TABLE if not exists Tab_Lieux (
    PKId_Lieux SERIAL PRIMARY KEY,
    Nom TEXT NOT NULL,
    Longitude FLOAT,
    Latitude FLOAT,
    Photo TEXT NOT NULL,
    Adress_1 TEXT NOT NULL,
    Adress_2 TEXT NOT NULL,
    Adress_3 TEXT NOT NULL,
    Description TEXT NOT NULL,
    Accessibilite TEXT NOT NULL,
    Payant BOOLEAN default FALSE,
    Telephone TEXT NOT NULL,
    Horaires TEXT NOT NULL,
    Site_Web TEXT NOT NULL,
    Email TEXT NOT NULL,
    FKId_Fraicheur INT NOT NULL,
    FKCodeInsee_Ville TEXT NOT NULL,
    FOREIGN KEY(FKId_Fraicheur) REFERENCES Tab_Fraicheur(PKId_Fraicheur),
    FOREIGN KEY(FKCodeInsee_Ville) REFERENCES VilleFrance("CodCommune")
  )
"""

InsTab_Lieux = """
  INSERT INTO Tab_Lieux (
    Nom, 
    Longitude,
    Latitude,
    Photo,
    Adress_1,
    Adress_2,
    Adress_3,
    Description,
    Accessibilite,
    Payant,
    Telephone,
    Horaires,
    Site_Web,
    Email,
    FKId_Fraicheur, 
    FKCodeInsee_Ville
  )
  VALUES (
    %(Nom)s,
    %(Longitude)s,
    %(Latitude)s, 
    %(Photo)s,
    %(Adress_1)s,
    %(Adress_2)s, 
    %(Adress_3)s, 
    %(Description)s,
    %(Accessibilite)s,
    %(Payant)s, 
    %(Telephone)s,
    %(Horaires)s,
    %(Site_Web)s,
    %(Email)s,
    %(FKId_Fraicheur)s, 
    %(FKCodeInsee_Ville)s
  ) returning PKId_Lieux;
"""

Tab_Fraicheur = """
  CREATE TABLE if not exists Tab_Fraicheur (
    PKId_Fraicheur SERIAL PRIMARY KEY,
    Type TEXT NOT NULL
  )
"""

InsTab_Fraicheur = """
  INSERT INTO Tab_Fraicheur (Type)
  VALUES (%(Type)s) ;
"""

# Tab_Villes = """
#   CREATE TABLE if not exists Tab_Villes (
#     PKCodeInsee_Ville TEXT PRIMARY KEY,
#     Nom TEXT NOT NULL,
#     CodePostal TEXT NOT NULL
#   )
# """

# InsTab_Villes= """
#   INSERT INTO Tab_Villes (PKCodeInsee_Ville, Nom, CodePostal)
#   VALUES (%(PKCodeInsee_Ville)s, %(Nom)s,%(CodePostal)s) returning PKCodeInsee_Ville;
# """

Tab_Categorie_Lieux = """
  CREATE TABLE if not exists Tab_Categorie_Lieux (
    PKId_C_Lieux SERIAL PRIMARY KEY,
    Nom TEXT NOT NULL,
    FKId_Fraicheur INT NOT NULL,
    FOREIGN KEY(FKId_Fraicheur) REFERENCES Tab_Fraicheur(PKId_Fraicheur)
  )
"""

InsTab_Categorie_Lieux = """
  INSERT INTO Tab_Categorie_Lieux (Nom, FKId_Fraicheur)
  VALUES (%(Nom)s, %(FKId_Fraicheur)s) ;
"""

Tab_Categorie_User = """
  CREATE TABLE if not exists Tab_Categorie_User (
    PKId_C_User SERIAL PRIMARY KEY,
    Nom TEXT NOT NULL
  )
"""

InsTab_Categorie_User = """
  INSERT INTO Tab_Categorie_User (Nom)
  VALUES (%(Nom)s) ;
"""

Asso_C_Lieux_Lieux = """
  CREATE TABLE if not exists Asso_C_Lieux_Lieux (
    FKId_C_Lieux INT NOT NULL,
    FKId_Lieux INT NOT NULL,
    FOREIGN KEY(FKId_C_Lieux) REFERENCES Tab_Categorie_Lieux(PKId_C_Lieux),
    FOREIGN KEY(FKId_Lieux) REFERENCES Tab_Lieux(PKId_Lieux)
  )
"""

InsAsso_C_Lieux_Lieux = """
  INSERT INTO Asso_C_Lieux_Lieux (FKId_C_Lieux, FKId_Lieux)
  VALUES (%(FKId_C_Lieux)s, %(FKId_Lieux)s);
"""

Asso_Lieux_C_User = """
  CREATE TABLE if not exists Asso_Lieux_C_User (
    FKId_C_User INT,
    FKId_Lieux INT,
    FOREIGN KEY(FKId_C_User) REFERENCES Tab_Categorie_User(PKId_C_User),
    FOREIGN KEY(FKId_Lieux) REFERENCES Tab_Lieux(PKId_Lieux)
  )
"""

InsAsso_Lieux_C_User = """
  INSERT INTO Asso_Lieux_C_User (FKId_C_User, FKId_Lieux)
  VALUES (%(FKId_C_User)s, %(FKId_Lieux)s);
"""


liste_num_ref =[1851,1987,3952,4861,1930,3812,1927,1931,1932,1934,1983,5147,1984,4097,1999,4308,4883,5855,
        4521,1988,4096,5098,1989,1990,1991,1992,5100,4307,1985,1993,1994,3852,1995,1997,
        1986,1998,1799,2033,2034,2031,2032,2030,1894,1981,5253,1922,1923,1924,1925,4633,
        1768,2244,2211,2193,4999,2582,3907,2593,5605,3012,4957,156,147,149,186,153,
        171,139,4071,188,4090,3971,4321,4322,4089,4318,192,158,3407,3338,4640,3343,
        3342,3344,3345,3346,3347,3360,3404,3405,3403,4812,6010,3777,3400,3828,3713,3297,
        5368,3293,3294,3298,5255,4053,3149,3292,3148,5269,3237,3239,5893,3410,3411,3234,
        3742,39,5000,4170,40,55,56,75,2284,2335,2334,5857,2345,5097,5096,5656,
        2346,5858,4156,2330,2331,2332,5917,2307,2648,418,3525,3526,3527,3529,3530,3531,
        3535,3537,3538,3542,4074,3463,3461,3459,3458,2907,2899,2911,5580,5021,2914,2877,
        5268,2918,2888,2898,2908,2904,5148,4073,2910,2900,2903,600,727,728,893,5108,
        5073,1091,1102,1290,1040,1297,1078,822,5472,1505,1506,1507,1508,810,1093,804,
        852,697,1057,4759,4754,5069,721,5256,4761,4763,5012,5001,5013,5014,5015,5016,
        5017,5916,5018,5019,910,5020,1112,4537,5406,6099,4538,4075,6012,5939,4934,5107,
        5106,5116,2895,1259,3124,3172,1617,5057,5052,5974,5886,3703,4639,1671]

################ Tables étape 2 ################

################ Tables étape 3 ################

## next week, a new episode...