from . import DBTab 
from .BDfonction import connection, create_tab, insert_table, insert_assoc_table, drop_table, DeconnexionBD
from .CodeAPI.InterrogationAPI import interrogation_Apidae, dictionnaire_apidae

def DropTableDB(conn):
    
    drop_table(conn, "Asso_C_Lieux_Lieux")
    drop_table(conn, "Asso_Lieux_C_User")
    drop_table(conn, "Tab_Lieux")
    drop_table(conn, "Tab_Categorie_User")
    drop_table(conn, "Tab_Categorie_Lieux")
    drop_table(conn, "VilleFrance")
    drop_table(conn, "Tab_Fraicheur")

def init_tab_fraicheur(conn):
    list_fraicheur = ["a définir", "aéré", "frais", "froid", "très froid"]
    for fraicheur in list_fraicheur:
        dict_insert_Fraicheur = {
            "Type" : fraicheur,
        }
        PKId_Fraicheur = insert_table(conn, DBTab.InsTab_Fraicheur, dict_insert_Fraicheur)
        conn.commit()

def init_tab_categorie_user(conn):
    list_categorie_user = ["a définir", "Senior", "adulte", "jeune", "enfant", "solidaire"]
    for categorie_user in list_categorie_user:
        dict_insert_Categorie_User = {
            "Nom" : categorie_user
        }
        PKId_C_User = insert_table(conn, DBTab.InsTab_Categorie_User, dict_insert_Categorie_User)
        conn.commit()

def init_categorie_lieux(conn):
    list_categorie_lieux = ["a définir","Sport à la fraich", "Se divertir en fraîcheur", "Être à l’ombre en nature", "Être au contact de l'eau"]
    for categorie_lieu in list_categorie_lieux:
        dict_insert_Categorie_Lieux = {
            "Nom" : categorie_lieu,
            "FKId_Fraicheur" : 1
        }
        PKId_C_Lieux = insert_table(conn, DBTab.InsTab_Categorie_Lieux, dict_insert_Categorie_Lieux)
        conn.commit()

def InitialisationDB(conn):

    if conn is not None:
        with open("Packages/DB/Villes38_JS.sql") as MonFichier :
            sql = MonFichier.read()
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()

        create_tab(conn, DBTab.Tab_Fraicheur)
        create_tab(conn, DBTab.Tab_Categorie_Lieux)
        create_tab(conn, DBTab.Tab_Categorie_User)
        create_tab(conn, DBTab.Tab_Lieux)
        create_tab(conn, DBTab.Asso_Lieux_C_User)
        create_tab(conn, DBTab.Asso_C_Lieux_Lieux)  

        init_tab_fraicheur(conn)
        init_tab_categorie_user(conn)
        init_categorie_lieux(conn)

        liste_final=list()
        for i in range(len(DBTab.liste_num_ref)):
            liste=list()
            r = interrogation_Apidae(numero_critere=DBTab.liste_num_ref[i],total=True)
            liste= dictionnaire_apidae(r)
            liste_final.append(liste)

            for j in range(len(liste)):
                code_insee = liste_final[i][j]["lieu"]["code_insee"]
                list_CodeInsee = ""
                if code_insee[:2] == "38":
                    cur = conn.cursor()
                    try:
                        cur.execute(f"""SELECT "CodCommune" FROM VilleFrance WHERE "CodCommune" = '{code_insee}';""")
                        list_CodeInsee = cur.fetchone()[0]
                        if list_CodeInsee == code_insee:

                            longitude = None
                            if liste_final[i][j]["lieu"].get("longitude"):
                                if liste_final[i][j]["lieu"]["longitude"] != "inconnue":
                                    longitude = liste_final[i][j]["lieu"]["longitude"]

                            Latitude = None
                            if liste_final[i][j]["lieu"].get("lattitude"):
                                if liste_final[i][j]["lieu"]["lattitude"] != "inconnue":
                                    Latitude = liste_final[i][j]["lieu"]["lattitude"]

                            Photo = ""
                            if liste_final[i][j].get("Photos"):
                                Photo = liste_final[i][j]["Photos"][0][0]["liens"][0]["url"]

                            dict_insert_Lieux = {
                                "Nom" : liste_final[i][j]["Nom"] if liste_final[i][j].get("Nom") else "",
                                "Longitude" : longitude,
                                "Latitude" : Latitude,
                                "Photo" : Photo,
                                "Adress_1" : liste_final[i][j]["lieu"]["adresse_1"] if liste_final[i][j]["lieu"].get("adresse_1") else "",
                                "Adress_2" : liste_final[i][j]["lieu"]["adresse_2"] if liste_final[i][j]["lieu"].get("adresse_2") else "",
                                "Adress_3" : liste_final[i][j]["lieu"]["adresse_3"] if liste_final[i][j]["lieu"].get("adresse_3") else "",
                                "Description" : liste_final[i][j]["Description"] if liste_final[i][j].get("Description") else "",
                                "Accessibilite" : "",
                                "Payant" : False, 
                                "Telephone" : liste_final[i][j]["Telephone"] if liste_final[i][j].get("Telephone") else "",
                                "Horaires" : "",
                                "Site_Web" : liste_final[i][j]["Site_Web"] if liste_final[i][j].get("Site_Web") else "",
                                "Email" : liste_final[i][j]["Email"] if liste_final[i][j].get("Email") else "",
                                "FKId_Fraicheur" : 1, 
                                "FKCodeInsee_Ville" : liste_final[i][j]["lieu"]["code_insee"] if liste_final[i][j]["lieu"].get("code_insee") else ""                              
                            }
                            PKId_Lieux = insert_table(conn, DBTab.InsTab_Lieux, dict_insert_Lieux)
                            conn.commit()

                            # dict_insert_Asso_C_Lieux_Lieux = {
                            #     "FKId_C_Lieux" : PKId_C_Lieux,
                            #     "FKId_Lieux" : PKId_Lieux
                            # }
                            # print(dict_insert_Asso_C_Lieux_Lieux)
                            # insert_assoc_table(conn, DBTab.InsAsso_C_Lieux_Lieux, dict_insert_Asso_C_Lieux_Lieux)

                            # dict_insert_Asso_Lieux_C_User = {
                            #     "FKId_C_User" : PKId_C_User,
                            #     "FKId_Lieux" : PKId_Lieux
                            # }
                            # print(dict_insert_Asso_Lieux_C_User)
                            # insert_assoc_table(conn, DBTab.InsAsso_Lieux_C_User, dict_insert_Asso_Lieux_C_User)
                    except:
                        pass

