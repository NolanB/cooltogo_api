import psycopg2, os
from enum import Enum
from werkzeug.security import check_password_hash, generate_password_hash
from dotenv import load_dotenv

load_dotenv()
# dictionnaire recuperant les variables d'environnement 
conf = {
"enviro":os.getenv("DB_ENVIRO", None),
"dbname":os.getenv("DB_DBNAME", None),
"user":os.getenv("DB_USER", None),
"password":os.getenv("DB_PASSWORD", None),
"host":os.getenv("DB_HOST", None),
"port":os.getenv("DB_PORT", None)
}

class UserRoleEnum(str, Enum):
    user = "user"
    admin = "admin"


user_roles = [r.value for r in UserRoleEnum]
################ Connexion ################
def connection(*,dbname=conf["dbname"], user=conf["user"], password=conf["password"], host=conf["host"], port=conf["port"]):
    try:
        conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
        print("co ok")
    except:
        conn= None

        print("Connexion impossible. Tu as bien lancé docker compose ?")
        print("Paramétres de connexions :", conf["enviro"])
    return conn


def DeconnexionBD(conn):
    try:
        conn.close()
    except:
        print('la deconnexion à échoué')

def select_donnee(CodeSQL:str, MonTuple:list)->list:
    
    conn,cur = Connexion()
    try:
        cur.execute(CodeSQL, MonTuple)
    except:
        print('la requete à échoué')
    DeconnexionBD(conn)


def supprimer_table(nom_table : str) -> None:
    '''
    supprime une table de la base de données
    nom_table: nom de la table a supprimer
    '''
    conn,cur =Connexion()
    nom_table=str(nom_table)
    q = """ DROP TABLE %s """
    with conn:
        try:
            cur.execute(q,nom_table)
            print(f'la table {nom_table} à bien était détruite')
        except Error:
            print(Error)
    DeconnexionBD(conn)


    #########################   UTILISATEUR   #########################""
def get_utilisateur(conn, user_id:int)->list:
    '''
    recupere les information d'un utilisateur
    '''
    user = None

    query = """
        SELECT * FROM users
        WHERE id = %s
    """

    try:
        with conn:
            cur=conn.cursor()
            cur.execute(query, (user_id,))
            user = cur.fetchone()
    except psycopg2.Error:
        raise ValueError

    return user

def authentification_utilisateur(conn, username, password):
    if not username or not password:
        raise ValueError

    user = None

    query = """
        SELECT * FROM users
        WHERE username = %s
    """

    try:
        with conn:
            cur=conn.cursor()
            cur.execute(query, (username,))
            user = cur.fetchone()
    except psycopg2.Error:
        raise ValueError
    
    if user and check_password_hash(user[2], password):
        return user

    raise ValueError

def create_db_schema(conn):
    '''
    Crée la table utilisateur
    '''
    if conn == None:
        print('probleme de conn')
    with conn:
        cur=conn.cursor()
        cur.execute(
            """
            CREATE TABLE if not exists  users (
                id SERIAL PRIMARY KEY,
                username VARCHAR(255) UNIQUE NOT NULL,
                password_hash VARCHAR(255) NOT NULL,
                role TEXT
            )
        """
        )

def drop_db_schema(conn):
    with conn:
        cur=conn.cursor()
        cur.execute("""DROP TABLE if exists users""")

def create_tab(conn, CodeSQL:str):
    if conn == None:
        print('probleme de conn')
        return Exception("Connexion Error")
    try:
        with conn:
            cur=conn.cursor()
            cur.execute(CodeSQL)         
            print("La création de table a réussi")
            return Exception("La création de table a réussi")
    except psycopg2.Error:
        print('La creation de la table a échoué')
        raise ValueError

def insert_table(conn, CodeSQL:str, dico_insert:dict):
    if conn == None:
        print('probleme de conn')
    else:
        try:
            with conn:
                cur = conn.cursor()
                cur.execute(CodeSQL, dico_insert)
                print("L'insertion dans la table a réussi")
                try:
                    result=cur.fetchone()[0]
                except:
                    result=None
                return result
        except psycopg2.Error:
            print("L'insertion dans la table a  échoué")
            raise ValueError

def insert_assoc_table(conn, CodeSQL:str, dico_insert:dict):
    if conn == None:
        print('probleme de conn')
    else:
        try:
            with conn:
                cur = conn.cursor()
                cur.execute(CodeSQL, dico_insert)
                print("L'insertion dans la table a réussi")
        except psycopg2.Error:
            print("L'insertion dans la table a  échoué")
            raise ValueError

def drop_table(conn, table:str):
    with conn:
        cur=conn.cursor()
        cur.execute(f"""DROP TABLE if exists {table}""")

def create_user(conn, username, password, role):
    '''
    fonction permettant la création d'un nouvelle utilisateur
    '''
    if not username or not password or role not in user_roles:
        raise KeyError

    query = """
        INSERT INTO users (username, password_hash, role)
        VALUES (%s,%s,%s);
    """

    password_hash = generate_password_hash(password)
    values = (username,password_hash,role)
    print(conn)
    try:
        with conn:
            cur=conn.cursor()
            cur.execute(query,values)
    except psycopg2.Error:
        raise ValueError

def recherche(mot_rechercher:str)->list:
    '''
    fonction renvoyant la liste de lieu comprenant mot_rechercher
    mot_rechercher: l'adresse ou le nom du lieu rechercher
    '''
    mot_rechercher=mot_rechercher.lower()
    conn = connection()
    cur= conn.cursor()
    sql_news=f"""sELECT PKId_Lieux, Nom, Description, Photo
        FROM Tab_Lieux
        where Nom like '%{mot_rechercher}%' 
        or Adress_1 like '%{mot_rechercher}%' 
        or Adress_2 like '%{mot_rechercher}%'
        or Adress_3 like '%{mot_rechercher}%';"""
    cur.execute(sql_news)
    liste_news=cur.fetchall()
    return liste_news

def liste_fraicheur()->list:
    conn = connection()
    cur= conn.cursor()
    sql_news=f"""sELECT PKId_Fraicheur, Type
        FROM Tab_Fraicheur
        ;"""
    cur.execute(sql_news)
    liste_fraicheur=cur.fetchall()
    return liste_fraicheur

def liste_categorie()->list:
    '''
    fonction renvoyant la liste de toute les categories de lieu
    '''
    conn = connection()
    cur= conn.cursor()
    sql_news=f"""sELECT PKId_C_Lieux, Nom
        FROM Tab_Categorie_Lieux
        ;"""
    cur.execute(sql_news)
    liste_categorie=cur.fetchall()
    return liste_categorie

def liste_categorie_user()->list:
    '''
    fonction renvoyant la liste de toute les categories d'utilisateur
    '''
    conn = connection()
    cur= conn.cursor()
    sql_news=f"""sELECT PKId_C_User, Nom
        FROM Tab_Categorie_User
        ;"""
    cur.execute(sql_news)
    liste_categorie_user=cur.fetchall()
    return liste_categorie_user

def inserer_categorie(id_lieu:int,id_categorie:int):
    '''
    id_lieu: id(base de donées) du lieux selectionner 
    id_categorie: l'id de la categorie (pres d'un lac ...)
    '''
    
    conn = connection()
    dict_insert_Categorie_Lieux = {
                "FKId_Lieux" : id_lieu,
                "FKId_C_Lieux" : id_categorie
            }
    InsAsso_C_Lieux_Lieux = """
        INSERT INTO Asso_C_Lieux_Lieux (FKId_C_Lieux, FKId_Lieux)
        VALUES (%(FKId_C_Lieux)s, %(FKId_Lieux)s);
        """
    insert_assoc_table(conn, InsAsso_C_Lieux_Lieux,dict_insert_Categorie_Lieux)

def update_fraicheur(id_lieu:int,id_fraicheur:int):
    '''
    fonction permettant de modofier la fraicheur pour un lieu données
    '''
    sql_update=''' UPDATE Tab_Lieux set FKId_Fraicheur= %s where PKId_Lieux = %s'''

    conn = connection()
    cur= conn.cursor()
    cur.execute(sql_update,(id_fraicheur,id_lieu))

def inserer_categorie_user(id_lieu:int,id_categorie_user:int):
    '''
    fonction permettant d'inserer une nouvelle categorie d'utilisateur dans la base de donnes
    '''
    conn = connection()

    dict_insert_Categorie_user = {
                "FKId_Lieux" : id_lieu,
                "FKId_C_User" : id_categorie_user
            }
    InsAsso_Lieux_C_User = """
        INSERT INTO Asso_Lieux_C_User (FKId_C_User, FKId_Lieux)
        VALUES (%(FKId_C_User)s, %(FKId_Lieux)s);
        """
    try:
        insert_assoc_table(conn,InsAsso_Lieux_C_User,dict_insert_Categorie_user)
    except:
        print("erreur d'insertion'")