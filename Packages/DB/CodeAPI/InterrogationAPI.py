import requests
import json
from html.parser import HTMLParser
def interrogation_Apidae(total:bool=False, numero_critere:int=1990, first:int=0, count:int=200)->list:
    '''
    total: booleen qui a true recupere tout les resultat et a false les 200 premier par default
    numero_critere: id de la categorie rechercher
    first: postion du premier resultat renvoyer
    count: nombre de resultat renvoyer par la fonction 
    '''
    critere=f"critereInterne:CritereInterne_{numero_critere}"
    requete='http://api.apidae-tourisme.com/api/v002/recherche/list-objets-touristiques?query={'f'"projetId" : "4364", "apiKey" : "ALrtqQmv", "criteresQuery":"{critere}", "first" : {first}, "count" : {count}''}'
    r = requests.get(requete)
    dico=r.json()
    if total == True:
        #numFound es le nombre de resultat trouvez renvoyer par apidae
        nombre_max=dico['numFound']
    else :
        nombre_max =count
    i=200
    liste=[]
    liste.append(dico)
    #boucle permettant de recuperer tout les resultats de apidae (les resultat de apidae son limite a 200 par appel)
    while i<nombre_max:
        i+=200
        first+=200
        requete='http://api.apidae-tourisme.com/api/v002/recherche/list-objets-touristiques?query={'f'"projetId" : "4364", "apiKey" : "ALrtqQmv", "criteresQuery":"{critere}", "first" : {first}, "count" : {count}''}'

        r = requests.get(requete)
        liste.append(r.json())
    return liste


def traitement_description(texte:str)->str:
    '''
    texte: chaine de caractere a traiter
    '''
    texte=texte.replace('\n','')
    texte=texte.replace('\t','')
    texte=texte.replace('\r','')
    texte=texte.replace('\s','')
   

    return texte

def dico_activite_infoActivite(objet):
    '''
    Création d'un dictionnaire recuperant les information des activite d'apidae
    '''
    liste1=list(objet.keys())
    # print(liste1)
    dico_activite=dict()
    if 'activitesCulturelles' in liste1:
        dico_activite=activitesCulturelles(objet['activitesCulturelles'])
    if "activitesSportives" in liste1:      
        dico_activite=activitesCulturelles(objet['activitesSportives'])
    if 'activiteType' in liste1:
        # print(objet['activiteType'])
        try:
            dico_activite['activite_type']=objet['informationsActivite']['activiteType']['libelleFr']
        except:
            dico_activite['activite_type']='inconnue'
    
    if 'typesDetailles' in liste1:    
        
        for e in objet['typeDetailles']:
            try:   
                dico_activite['activite_type']=e['libelleFr']
                dico_activite['famillecritere']=e['familleCritere']['id']

            except:
                dico_activite['activite_type']='inconnue'
    try:
        dicos=commune(objet['commerceEtServicePrestataire'])
    except:
        dicos="inconnu"
    
    dico_activite['commune']=dicos
    return dico_activite


def activitesCulturelles(objet:list):
                            
    try:
        for elem in objet:
            dico_activite=dict()
            try:
                dico_activite['nom']=elem['libelleFr'].lower()
            except:
                dico_activite['nom']='inconnu'
        
            try:
                dico_activite['description']=traitement_description(elem['description'])
            except:
                dico_activite['description']='inconnue'
            
            #dico_activite['nom']=objet['informationsActivite']['activitesCulturelles']['libelleFr']
        dico_activite['existe']=True
    except:
        dico_activite['existe']=False
    return dico_activite


def moyencomm(objet:list,dico:dict)->dict:
    '''
    crée un dictionnaire recuperant les different moyen de communication pour un lieu
    '''
    for comm in objet:
        if comm['type']['libelleFr'] =="Téléphone":
            dico['Telephone']=comm['coordonnees']['fr']
        elif comm['type']['libelleFr'] =="Mél":
            dico['Email']=comm['coordonnees']['fr']
        elif comm['type']['libelleFr'] =="Site web (URL)":
            dico['Site_Web']=comm['coordonnees']['fr']
        else:
            dico['Autre_contact']=comm['coordonnees']['fr']
    return dico

def illustation(objet:list)->dict:
    '''
    crée un dictionnaire comprenant les informations des photos lier a un leiu
    '''
    liste=list()
    for i in objet:
        #['identifiant', 'link', 'type', 'nom', 'legende', 'copyright'
        # , 'traductionFichiers', 'locked']
        #print(f"l'id de l'illustation es {i['identifiant']}")
        photo=dict()
        photo['nom_photo']=i['nom']['libelleFr'].lower()
        photo['liens']=list()

        try:
            for trad in i['traductionFichiers']:
                lien=dict()
                #['locale', 'url', 'urlListe', 'urlFiche', 'urlDiaporama', 'extension', 'fileName', 'taille', 'hauteur', 'largeur', 'lastModifiedDate']
                lien['url']=trad['url']
                lien['date_modif']=trad['lastModifiedDate']
                lien['info']=True
                photo['liens'].append(lien)
                #print(f"l'extension es {trad['extension']}")
        except:
            lien=dict()
            lien['info']=False
            photo['liens'].append(lien)
        photo['existe']=True
        liste.append(photo)
    return liste

def commune(objet):
    '''
    Création d'un dictionnaire recuperant tout les information d'un lieu sur apidae
    '''
    dicos=dict()    
    for cles in objet.keys():
       
        try:
            dicos['code_postal']=objet['adresse']['codePostal']
        except:
            dicos['codepostal']='inconnu'
        try:
            dicos['nom_commune']=objet['adresse']['commune']['nom'].lower()
        except:
            dicos['nom_commune']='inconnu'
        try:
            dicos['code_insee']=objet['adresse']['commune']['code']
        except:
            dicos['code_insee']='inconnu'
        try:
            dicos['adresse_1']=objet['adresse']['adresse1'].lower()
        except:
            
            dicos['adresse_1']='inconnue'
        try:
            dicos['adresse_2']=objet['adresse']['adresse2'].lower()
        except:
            dicos['adresse_2']='inconnue'
        try:
            dicos['adresse_3']=objet['adresse']['adresse3'].lower()
        except:
            dicos['adresse_3']='inconnue'
        try:
            dicos['longitude']=objet['geolocalisation']['geoJson']['coordinates'][0]
        except:
            dicos['longitude']='inconnue'
        try:
            dicos['lattitude']=objet['geolocalisation']['geoJson']['coordinates'][1]
        except:
            
            dicos['lattitude']='inconnue'

    return dicos
def dictionnaire_apidae(r):
    liste_final=list()
    reponse=r[0]['numFound']
    # print(r[0]['numFound'])
    if int(reponse)!=0:
        #print(r[0].keys())
        #['query', 'numFound', 'objetsTouristiques', 'formatVersion']
        #print(r[0]['query'].keys())
        #['searchFields', 'criteresQuery', 'first', 'count', 'order', 'asc', 'apiKey', 'projetId']
        #['type', 'id', 'nom', 'identifier', 'informations', 'presentation', 'localisation', 'illustrations', 'informationsActivite']
        
        for rep in range(len(r)):
            for objet in r[rep]['objetsTouristiques']:

                listecle= list(objet.keys())

                dico=dict()
                dico['type']=objet['type']
                #print(f"le type de l'objettourstique es  : commerce_service,activite etc...")
                #print(f"l'id' de l'objettouristique es  : {objet['id']}")
                dico['Nom']=objet['nom']['libelleFr'].lower()
                
                try:
                    dico=moyencomm(objet['informations']['moyensCommunication'],dico)
                    dico['contact']=True
                        
                except:
                    dico['contact']=False
                try:
                    dico['Description']=traitement_description(str(objet['presentation']['descriptifCourt']['libelleFr']))
                except:
                    dico['Description']=False
                try:
                    dico['Photos']=list()
                    liste_illu=illustation(objet['illustrations'])
                    
                    dico['Photos'].append(liste_illu)

                except:
                    photo=dict()
                    photo['existe']=True
                    dico['Photos'].append(photo)

                if 'informationsActivite' in listecle:
                    dico_activite=dico_activite_infoActivite(objet['informationsActivite'])    
                    dico['activite']=dico_activite
                    
                if 'localisation' in listecle:                        
                    dico['lieu']=commune(objet['localisation'])

                liste_final.append(dico)
    return(liste_final)


if __name__ == "__main__":
        
    liste_num_ref =[1851,1987,3952,4861,1930,3812,1927,1931,1932,1934,1983,5147,1984,4097,1999,4308,4883,5855,
    4521,1988,4096,5098,1989,1990,1991,1992,5100,4307,1985,1993,1994,3852,1995,1997,
    1986,1998,1799,2033,2034,2031,2032,2030,1894,1981,5253,1922,1923,1924,1925,4633,
    1768,2244,2211,2193,4999,2582,3907,2593,5605,3012,4957,156,147,149,186,153,
    171,139,4071,188,4090,3971,4321,4322,4089,4318,192,158,3407,3338,4640,3343,
    3342,3344,3345,3346,3347,3360,3404,3405,3403,4812,6010,3777,3400,3828,3713,3297,
    5368,3293,3294,3298,5255,4053,3149,3292,3148,5269,3237,3239,5893,3410,3411,3234,
    3742,39,5000,4170,40,55,56,75,2284,2335,2334,5857,2345,5097,5096,5656,
    2346,5858,4156,2330,2331,2332,5917,2307,2648,418,3525,3526,3527,3529,3530,3531,
    3535,3537,3538,3542,4074,3463,3461,3459,3458,2907,2899,2911,5580,5021,2914,2877,
    5268,2918,2888,2898,2908,2904,5148,4073,2910,2900,2903,600,727,728,893,5108,
    5073,1091,1102,1290,1040,1297,1078,822,5472,1505,1506,1507,1508,810,1093,804,
    852,697,1057,4759,4754,5069,721,5256,4761,4763,5012,5001,5013,5014,5015,5016,
    5017,5916,5018,5019,910,5020,1112,4537,5406,6099,4538,4075,6012,5939,4934,5107,
    5106,5116,2895,1259,3124,3172,1617,5057,5052,5974,5886,3703,4639,1671]
    liste_final=list()

    for i in range(7):
       
        r = interrogation_Apidae(numero_critere=liste_num_ref[i],total=True)
        liste=dictionnaire_apidae(r)
        # if len(liste)>=1:
        #     print(liste[0]['lieu'])
        liste_final.append(liste)
        #print(liste_final)
        # print(" CA COMMENCE  ")
        # print(liste_final)
        # print(" CA FINI  ")
    